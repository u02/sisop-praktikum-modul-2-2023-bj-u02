#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

void print_usage(char *program_name) {
    printf("Usage: %s <hour> <minute> <second> <*> <file>\n", program_name);
}

int is_valid_argument(char *arg) {
    if (strcmp(arg, "*") == 0) {
        return 1;
    }

    int len = strlen(arg);
    for (int i = 0; i < len; i++) {
        if (!isdigit(arg[i])) {
            return 0;
        }
    }

    int value = atoi(arg);
    if (value < 0 || value > 59) {
        return 0;
    }

    return 1;
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Error: Invalid number of arguments\n");
        print_usage(argv[0]);
        exit(1);
    }

    char *hour_arg = argv[1];
    char *minute_arg = argv[2];
    char *second_arg = argv[3];
    char *file = argv[5];

    // Check if the given arguments are valid
    if (!is_valid_argument(hour_arg) || !is_valid_argument(minute_arg) || !is_valid_argument(second_arg)) {
        printf("Error: Invalid argument format\n");
        print_usage(argv[0]);
        exit(1);
    }

    // Fork a child process to run the command in the background
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(1);
    } else if (pid == 0) {
        // Child process
        char *cmd_args[] = {"bash", file, NULL};
        char *envp[] = {NULL};
        execve("/bin/bash", cmd_args, envp);
        perror("execve");
        exit(1);
    } else {
        // Parent process
        // Wait for the child process to finish
        int status;
        if (waitpid(pid, &status, 0) == -1) {
            perror("waitpid");
            exit(1);
        }
    }

    return 0;
}
