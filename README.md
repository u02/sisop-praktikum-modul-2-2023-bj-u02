<h1>Praktikum Modul 1 Sistem Operasi</h1>
<h3>Group U02</h3>

| NAME                      | NRP       |
|---------------------------|-----------|
|Pascal Roger Junior Tauran |5025211072 |
|Riski Ilyas                |5025211189 |
|Armstrong Roosevelt Zamzami|5025211191 |


## Number 1
```
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut :
```
### 1A

```
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

```
Solution:<br>
```c
void download() {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", ZIP_FILENAME, URL, NULL);
        printf("Error: Failed to execute wget. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }
}

void extract() {
    pid_t pid;
    int status;
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", ZIP_FILENAME, NULL);
        printf("Error: Failed to execute unzip. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }

}
```
Exlpanation:
- `download()` is the function in which will be called firstly to execute the downloading of the animal images.
- `pid = fork()` used to spawn child process to start the download.
- `execl` is the execution to download the images. This is called within child process and has argument `-q` & `-O`, then followed by the `ZIP_FILENAME` & `URL`
- `extract()` is the function to unzip the downloaded files after function `download()` called.
- `pid = fork()` used to spawn child process to start the unzip.
- `execl` is used since `system()` is not allowed. it is an execution to unzip the file.

### 1B
```
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
```
Solution:<br>
```c
void arrange_animal(int i) {
    DIR *dir;
    struct dirent *ent;
    char path[100] = "/home/riskiilyas/Desktop/SISOP/";
    char dir_path[100] = "/home/riskiilyas/Desktop/SISOP/";
    strcat(dir_path, POSISI[i]);
    char buffer[256];
    char *token;
    const char *delimiter = "_";
    mkdir(POSISI[i], S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Failed to open directory.\n");
        exit(EXIT_FAILURE);
    }

    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
            continue;
        }
        strcpy(buffer, ent->d_name);
        token = strtok(buffer, delimiter);
        while (token != NULL) {
            if (strcmp(token, FILE_CATEGORY[i]) == 0) {
                char file_path[100];
                strcpy(file_path, path);
                strcat(file_path, ent->d_name);
                printf("%s", file_path);
                int pid = fork();
                if(pid == 0) {
                    char *argv2[] = {"cp", file_path, dir_path, NULL};
                    execv("/bin/cp", argv2);
                } else {
                    wait(NULL);
                }
                remove(file_path);
                break;
            }
            token = strtok(NULL, delimiter);
        }
    }

    closedir(dir);
}
```
Explanation:
- `arrange_animal()` is the function to choose and arrange all the animals.
- `path` is the path of the animals image files 
- `dir_path` is the directory of the files that will be arranged.
- `const char *delimiter = "_"` is the delimiter because the images have to be parsed by '_' symbol
- `mkdir` is called because the animals will be categorized based on the habitat such as `"HewanAir", "HewanAmphibi", "HewanDarat"`
- `dir = opendir(path)` is called to read the files within `path`
- `while ((ent = readdir(dir)) != NULL)` is the looping to traverse through all the files that will be arranged later on
- `while (token != NULL)` is the looping to parse each files name
- `execv("/bin/cp", argv2)` is to copy the image files to the folder
- `remove(file_path)` is called to delete the file after copied to the destined folder

### 1C
```
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
```
Solution:<br>
```c

void classify(){
    pid_t pid[3];
    for(int i=0 ;i<3; i++) {
        pid[i] = fork();
        if(pid[i]==-1) {
            perror("Failed to Create Proccess!!");
            exit(EXIT_FAILURE);
        } else if(pid[i]==0) {
            arrange_animal(i);
            exit(EXIT_SUCCESS);
        }
    }

    for(int i=0;i<3;i++) {
        waitpid(pid[i], NULL, 0);
    }
}

```
Explanation:
- `classify()` is the function to categorize the images based on the habitat.
- `pid_t pid[3]` is to store the processes pid because each category will be executed with different child process.
- `for(int i=0 ;i<3; i++)`is looping to process the classification with 3 different processes
- `arrange_animal(i)` is the call to `arrange_animal()` function that has been created at `1B`
- `waitpid(pid[i], NULL, 0)` is to wait all the child process to run so that the program can end successfully

### 1D
```
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.
```
Solution:<br>
```c
void zip(){
    char zipFileName[] = "grapekun.zip";
    char* cmd[] = {"zip", "-r", zipFileName, POSISI[0], POSISI[1], POSISI[2], NULL};
    execvp("zip", cmd);
}
```
Explanation:
- `zip()` is the function to zip all the classified images into 1 zip file
- `char zipFileName[] = "grapekun.zip"` is the name of the zip file.
- `execvp("zip", cmd)` is to start executing the command above.

##  Number2
```
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !
```
### 2A
```
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
```
Solution:<br>
```sh
#define FOLDER_NAME_FORMAT "%Y-%m-%d_%H:%M:%S"
#define FOLDER_CREATE_INTERVAL 30

void create_folder() {
    time_t current_time;
    struct tm* time_info;
    char folder_name[80];

    time(&current_time);
    time_info = localtime(&current_time);
    strftime(folder_name, sizeof(folder_name), FOLDER_NAME_FORMAT, time_info);
    // created the folder
    mkdir(folder_name, 0700);
    // Change current folder_name to the newly created folder
    chdir(folder_name);
}
```
Explanation:
- `FOLDER_NAME_FORMAT` has with a format string "%Y-%m-%d_%H:%M:%S". This format string represents the date and time in the format  of YYYY-MM-DD_HH:MM:SS.
- `FOLDER_CREATE_INTERVAL` has a value of 30. This variable is used to space out the creation of the folders into 30 second intervals.
- `localtime` function with the address of current_time as an argument to convert the time_t value to a struct tm* value representing the local time.
-  `strftime` function with the `folder_name` buffer, its size, a format string `FOLDER_NAME_FORMAT`, and the `time_info` structure is used to format the time as a string and store it in `folder_name`.
-  `mkdir` function with the `folder_name` as the name of the folder and a permission of `0700` to create the folder with read, write, and execute permissions for the owner only.
- `chdir` function with the `folder_name` as the new current working directory to change the current directory to the newly created folder.
### 2B
```
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
```
Solution:<br>
```sh
#define FILE_NAME_FORMAT   "%Y-%m-%d_%H:%M:%S.jpg"
#define MAX_PHOTOS 15

void download_photos() {
    int photo_count = 0;
    while (photo_count < MAX_PHOTOS) {
        time_t current_time;
        struct tm* time_info;
        char file_name[80];
        char url[50];

        time(&current_time);
        time_info = localtime(&current_time);
        strftime(file_name, sizeof(file_name), FILE_NAME_FORMAT, time_info);
        sprintf(url, "https://picsum.photos/%d", (photo_count % 10) + 400);

        FILE* file = fopen(file_name, "wb");
        if (file == NULL) {
            perror("Failed to create file");
            exit(EXIT_FAILURE);
        }

        printf("Downloading %s...\n", file_name);
        fflush(stdout);

        pid_t pid = fork();
        if (pid < 0) {
            perror("Failed to fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            // Child process
			//execl("/usr/bin/wget", "wget", "-q", "-O", file_name, url, NULL);
            char size_option[20];
            sprintf(size_option, "%dx%d", (int)(current_time % 1000) + 50, (int)(current_time % 1000) + 50);
            execl("/usr/bin/wget", "wget", "-q", "-O", file_name, url, NULL);
            execl("/usr/bin/convert", "convert", "-resize", size_option, file_name, file_name, NULL);
        } else {
            // Parent process
            fclose(file);
            photo_count++;
            sleep(DOWNLOAD_INTERVAL);
        }
    }
    // Wait for child processes to complete
    while (wait(NULL) > 0) {}
}
```
Explanation:
- `photo_count` variable is used to keep track of the number of photos that have been downloaded.
- The function enters a while loop that continues as long as `photo_count` is less than `MAX_PHOTOS` which is 15.
- The variable `current_time` of type `time_t` to store the current time which will be used to name the file. 
- The `file_name` will be used to store the filename which will be in the format of YYYY-MM-DD_HH:MM:SS.jpg
- The array `url` of size 50 to store the URL of the photo to be downloaded.
- We get the current time using the `time` function and stores it in `current_time`.
- We convert the `time_t` value to a `struct tm*` value representing the local time using the `localtime` function and stores it in `time_info`.
- We format the local time as a string using the `strftime` function and the `FILE_NAME_FORMAT` macro.
- We open a file with the name  `file_name` in write binary mode using the `fopen` function and stores the file pointer in file.
- We fork a new process using the fork function and store the process ID in `pid`.
- If `pid` is equal to 0, we execute the `execl` function to run the `wget` command with the options "-q", "-O", `file_name`, and `url`.
- We execute another `execl` function to run the `convert` command with the options "-resize", the size of the image to be resized.
- If `pid` is not equal to 0, closes the file using the `fclose` function, increments `photo_count`, sleeps for `DOWNLOAD_INTERVAL` seconds, and goes back to the beginning of the while loop.

### 2C
```
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
```
Solution:<br>
```sh

void zip(char *folder_name) {
    pid_t pid;
    int status;

    // Construct the command to zip the folder_name
    char *zipArgs[] = {"zip", "-r", "-q", "-9", folder_name, folder_name, NULL};

    // Fork a child process to run the zip command
    pid = fork();
    if (pid == 0) {
        execvp(zipArgs[0], zipArgs);
        perror("zip error");
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        perror("fork error");
        exit(EXIT_FAILURE);
    }

    // Wait for the zip process to finish
    waitpid(pid, &status, 0);
}
```

Explanation:
- Construct a string array that represents the command and arguments for the `zip` function. The program will zip the contents of the folder specified by `folder_name` into a new archive with the same name as the folder.
- Fork a new process and returns the process ID `pid`.
- If the `pid` equals 0, the process is the child process. The child process uses the `execvp` function to execute the `zip` function with the arguments specified in the `zipArgs` array. 
- The parent process waits for the child process to finish using the `waitpid` function, which returns the status of the child process.
- Sadly the zip function does not work for the program and I do not know why. It does not return an error and the program compiles without any issue.

### 2D
```
Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

```
Solution:<br>
```sh
void generate_stop_script() {
  char kill_prog[] = "lukisan";
    FILE* script_file = fopen("stop_daemon.sh", "w");
    if (script_file == NULL) {
        perror("Failed to create stop script file");
        exit(EXIT_FAILURE);
    }

    fprintf(script_file, "#!/bin/sh\n");
    fprintf(script_file, "kill $(pgrep -f %s)\n", kill_prog);
    fprintf(script_file, "rm -f stop_daemon.sh\n");

    if (fclose(script_file) != 0) {
        perror("Failed to close stop script file");
        exit(EXIT_FAILURE);
    }

    if (chmod("stop_daemon.sh", 0700) != 0) {
        perror("Failed to set stop script file permissions");
        exit(EXIT_FAILURE);
    }

    printf("Kill script generated... \n");
}
```

Explanation:
- Open the file `stop_daemon.sh` in write mode using the `fopen` function and returns a file pointer to the opened file. 
- Write the first line of the script, which indicates that the script should be executed using shell script.
- Writes the second line of the script, which uses the `pgrep` command to find the process ID of the daemon process based on its name, which is `lukisan`. The `kill` command is then used to terminate the.
- Write the third line of the script, which removes the `stop_daemon.sh` file to clean up after itself and not leave anything behind.

### 2E
```
Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
```
Solution:<br>
```sh
```

Explanation:


## Number3
```
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
```
## 3A
```
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
```
Solution:<br>

```c

void download() {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", ZIP_FILENAME, URL, NULL);
        printf("Error: Failed to execute wget. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }
}

void extract() {
    pid_t pid;
    int status;
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", ZIP_FILENAME, NULL);
        printf("Error: Failed to execute unzip. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }

}

void remove_zip() {
    pid_t pid;
    int status;
    pid = fork();
    if (pid == 0) {
        execl("/bin/rm", "rm", ZIP_FILENAME, NULL);
        printf("Error: Failed to execute rm. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }
}

```

Explanation:
- `download()` is the function in which will be called firstly to execute the downloading of the player images.
- `pid = fork()` used to spawn child process to start the download.
- `execl` is the execution to download the images. This is called within child process and has argument `-q` & `-O`, then followed by the `ZIP_FILENAME` & `URL`
- `extract()` is the function to unzip the downloaded files after function `download()` called.
- `pid = fork()` used to spawn child process to start the unzip.
- `execl` is used since `system()` is not allowed. it is an execution to unzip the file.
- `remove_zip()` is the function to remove the zip that has been extracted.
- `pid = fork()` used to spawn child process to start the unzip.
- `execl("/bin/rm", "rm", ZIP_FILENAME, NULL)` is the execution call to remove the zip and called within child proccess.

## 3B
```
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
```
Solution:<br>

```c

void delete_non_manutd(){
      DIR *dir;
    struct dirent *ent;
    char path[100] = "players/";
    char buffer[256];
    char *token;
    const char *delimiter = "_";

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Failed to open directory.\n");
        exit(EXIT_FAILURE);
    }

    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
            continue;
        }

        strcpy(buffer, ent->d_name);
        token = strtok(buffer, delimiter);
        while (token != NULL) {
            if (strcmp(token, "ManUtd") == 0) {
                break;
            }
            token = strtok(NULL, delimiter);
        }

        if (token == NULL) {
            char file_path[100];
            strcpy(file_path, path);
            strcat(file_path, ent->d_name);
            remove(file_path);
        }
    }

    closedir(dir);
}


```

Explanation:<br>
- `delete_non_manutd()` is the function delete all the players of non Manchester United
- `char path[100] = "players/"` is the desired path to check the players.
- `dir = opendir(path)` is called to read the files within `path`
- `while ((ent = readdir(dir)) != NULL)` is the looping to traverse through all the files that will be arranged later on
- `while (token != NULL)` is the looping to parse each files name
- `if (strcmp(token, "ManUtd") == 0)` this is to check if the player is Manchester United Player. if `true`, then the looping will be skipped and jump to other files.
- `if (token == NULL)` is to check if it's already at the and of the file name parsing, then to program will delete the file.
- `remove(file_path)` is the function call to actually remove the file.

### 3C
```
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
```
Solution:<br>
```c

void arrange_player(int i) {
    DIR *dir;
    struct dirent *ent;
    char path[100] = "players/";
    char dir_path[100] = "/home/riskiilyas/Desktop/SISOP/players/";
    strcat(dir_path, POSISI[i]);
    char buffer[256];
    char *token;
    const char *delimiter = "_";

    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"mkdir", "-p", dir_path, NULL};
        execv("/bin/mkdir", argv);
    } else {
        wait(NULL);
    }

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Failed to open directory.\n");
        exit(EXIT_FAILURE);
    }

    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
            continue;
        }

        strcpy(buffer, ent->d_name);
        token = strtok(buffer, delimiter);
        while (token != NULL) {
            if (strcmp(token, POSISI[i]) == 0) {
                char file_path[100];
                strcpy(file_path, path);
                strcat(file_path, ent->d_name);
                pid = fork();
                if(pid == 0) {
                    char *argv2[] = {"cp", file_path, dir_path, NULL};
                    execv("/bin/cp", argv2);
                } else {
                    wait(NULL);
                }
                remove(file_path);
                break;
            }
            token = strtok(NULL, delimiter);
        }
    }

    closedir(dir);
}

void classify(){
    pid_t pid[4];
    for(int i=0 ;i<4; i++) {
        pid[i] = fork();
        if(pid[i]==-1) {
            perror("Failed to Create Proccess!!");
            exit(EXIT_FAILURE);
        } else if(pid[i]==0) {
            arrange_player(i);
            exit(EXIT_SUCCESS);
        }
    }

    for(int i=0;i<4;i++) {
        waitpid(pid[i], NULL, 0);
    }
}

void count_files_number(int i){
    DIR *dir;
    struct dirent *entry;
    char path[50]= "players/";
    strcat(path, POSISI[i]);

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error opening directory\n");
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            PEMAIN_COUNT[i]++;
        }
    }
    closedir(dir);
}

int compare(const void *a, const void *b) {
    Player *playerA = (Player*)a;
    Player *playerB = (Player*)b;
    return playerB->score - playerA->score;
}

void swap(Player *xp, Player *yp) {
    Player temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(Player arr[], int n) {
    int i, j;
    for (i = 0; i < n-1; i++) {     
        for (j = 0; j < n-i-1; j++) {
            if (arr[j].score < arr[j+1].score) {
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

void sort_top_player(int i) {
    DIR *dir;
    struct dirent *ent;
    int fileCount = 0;
    char path[50]= "players/";
    strcat(path, POSISI[i]);
    char buffer[256];
    char *token;
    const char *delimiter = "_";

    if ((dir = opendir(path)) != NULL) {
        int ctr = 0;
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            strcpy(buffer, ent->d_name);
            token = strtok(buffer, delimiter);
            strcpy(PEMAIN[i][ctr].name, token);
            int token_ctr=0;
            while (token != NULL) {
                if(token_ctr==3) {
                    char skor[3];                    
                    strncpy(skor, token, 2);
                    skor[2] = '\0';
                    int score = atoi(skor);
                    PEMAIN[i][ctr].score = score;
                }
                token_ctr++;
                token = strtok(NULL, delimiter);
            }
            ctr++;
        }

        bubbleSort(PEMAIN[i], PEMAIN_COUNT[i]);
        closedir(dir);
    } else {
        printf("Unable to open directory\n");
    }
}

```
- `arrange_player()` is the function to choose and arrange all the players.
- `path` is the path of the players image files 
- `dir_path` is the directory of the files that will be arranged.
- `const char *delimiter = "_"` is the delimiter because the images have to be parsed by '_' symbol
- `mkdir` is called because the animals will be categorized based on the habitat such as `"Kiper", "Bek", "Gelandang", "Penyerang"`
- `dir = opendir(path)` is called to read the files within `path`
- `while ((ent = readdir(dir)) != NULL)` is the looping to traverse through all the files that will be arranged later on
- `while (token != NULL)` is the looping to parse each files name
- `execv("/bin/cp", argv2)` is to copy the image files to the folder
- `remove(file_path)` is called to delete the file after copied to the destined folder
- `classify()` is the function to categorize the images based on the the player category.
- `pid_t pid[3]` is to store the processes pid because each category will be executed with different child process.
- `for(int i=0 ;i<3; i++)`is looping to process the classification with 3 different processes
- `arrange_player(i)` is the call to `arrange_player()` function that has been created at before
- `waitpid(pid[i], NULL, 0)` is to wait all the child process to run so that the program can end successfully
- `count_files_number()` is function to count the number of players from Manchester United.
- `while ((entry = readdir(dir)) != NULL)` is the function to traverse each files. in each looping, there will be checking `if (entry->d_type == DT_REG)` if the file is regular file, then the count willl be incremented `PEMAIN_COUNT[i]++`
- `int compare(const void *a, const void *b)` is function to compare which one is larger in points
- `void swap(Player *xp, Player *yp)` is to swap the position of the two players inside the global array `PEMAIN`
- `void bubbleSort(Player arr[], int n)` is bubble sort algorithm to sort the players based on the points
- `void sort_top_player(int i)` is the function to sort the players from each category from the top points to the lowest point. THe functionality inside this function is quite the same with `arrange_player()` function that will be read all the player files and store it to the global array, then it will sort the players using `bubbleSort()` that is created before.

Explanation:


### 3D
```
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
```
Solution:<br>
```c

void buatTim(int bek, int gelandang, int striker) {
    char filename[256];
    sprintf(filename, "/home/riskiilyas/Formasi_%d_%d_%d.txt", bek, gelandang, striker);

    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("File tidak dapat dibuat\n");
        exit(1);
    }

    fprintf(fp, "Kiper:\n%s %d\n", PEMAIN[0][0].name, PEMAIN[0][0].score);
    
    fprintf(fp, "\nBek:\n");
    for(int i=0; i<bek; i++) {
        fprintf(fp, "%s %d\n", PEMAIN[1][i].name, PEMAIN[1][i].score);
    }

    fprintf(fp, "\nGelandang:\n");
    for(int i=0; i<gelandang; i++) {
        fprintf(fp, "%s %d\n", PEMAIN[2][i].name, PEMAIN[1][i].score);
    }

    fprintf(fp, "\nPenyerang:\n");
    for(int i=0; i<striker; i++) {
        fprintf(fp, "%s %d\n", PEMAIN[3][i].name, PEMAIN[1][i].score);
    }
 
    fclose(fp);
}

void kesebelasan(){
   for(int i=0; i<4; i++) {
    count_files_number(i);
    sort_top_player(i);
    printf("\n");
   }


   int team[4] = {0};
   team[0]++;

    int ctr=1;

    while(ctr<11) {
        int max=0, id=-1;
        for(int j=1; j<4; j++) {
            if(PEMAIN[j][team[j]].score > max) {
                max=PEMAIN[j][team[j]].score;
                id=j;
            }
        }
        if(id!=-1)team[id]++;
        ctr++;
    }

    buatTim(team[1], team[2], team[3]);
}

```

Explanation:
- `void buatTim(int bek, int gelandang, int striker)` is the function to store the selected players to a txt file inside `/home` directory.
- `sprintf(filename, "/home/riskiilyas/Formasi_%d_%d_%d.txt", bek, gelandang, striker)` is to create the txt file and the name is based on the number of player for `bek, gelandang, & striker`
- `FILE *fp = fopen(filename, "w")` is to open the txt file with `'w'` to set the mode to `MODE_WRITE`
- `fprintf(fp, "Kiper:\n%s %d\n", PEMAIN[0][0].name, PEMAIN[0][0].score)` is to print the Best Kiper
- `fprintf(fp, "%s %d\n", PEMAIN[1][i].name, PEMAIN[1][i].score)` is to print the Top Bek
- `fprintf(fp, "%s %d\n", PEMAIN[2][i].name, PEMAIN[2][i].score)` is to print the Top Gelandang
- `fprintf(fp, "%s %d\n", PEMAIN[3][i].name, PEMAIN[3][i].score)` is to print the Top striker
- `kesebelasan()` is the function to form the team formation
- `int team[4] = {0}` is the array to store number of top player for each category. each category is starting from 0. But the kiper will be incremented to 1 later on, because the kiper will be chosen only one
- `while(ctr<11)` is the looping to calculate the best players

## Number4
```
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.

```

Solution:<br>
```sh
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

void print_usage(char *program_name) {
    printf("Usage: %s <hour> <minute> <second> <*> <file>\n", program_name);
}

int is_valid_argument(char *arg) {
    if (strcmp(arg, "*") == 0) {
        return 1;
    }

    int len = strlen(arg);
    for (int i = 0; i < len; i++) {
        if (!isdigit(arg[i])) {
            return 0;
        }
    }

    int value = atoi(arg);
    if (value < 0 || value > 59) {
        return 0;
    }

    return 1;
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Error: Invalid number of arguments\n");
        print_usage(argv[0]);
        exit(1);
    }

    char *hour_arg = argv[1];
    char *minute_arg = argv[2];
    char *second_arg = argv[3];
    char *file = argv[5];

    // Check if the given arguments are valid
    if (!is_valid_argument(hour_arg) || !is_valid_argument(minute_arg) || !is_valid_argument(second_arg)) {
        printf("Error: Invalid argument format\n");
        print_usage(argv[0]);
        exit(1);
    }

    // Fork a child process to run the command in the background
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(1);
    } else if (pid == 0) {
        // Child process
        char *cmd_args[] = {"bash", file, NULL};
        char *envp[] = {NULL};
        execve("/bin/bash", cmd_args, envp);
        perror("execve");
        exit(1);
    } else {
        // Parent process
        // Wait for the child process to finish
        int status;
        if (waitpid(pid, &status, 0) == -1) {
            perror("waitpid");
            exit(1);
        }
    }

    return 0;
}
```

Explanation:
- The program takes the arguments `<hour>` `<minute>` `<second>` `<*>` and `<filename>` and turns it into a crontab.
