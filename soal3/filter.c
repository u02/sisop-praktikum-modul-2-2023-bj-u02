#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <dirent.h>
#define ZIP_FILENAME "file.zip"
#define URL "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"
char *POSISI[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};

typedef struct {
    char name[256];
    int score;
} Player;

Player PEMAIN[4][10];
int PEMAIN_COUNT[4] = {0};

void download() {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", ZIP_FILENAME, URL, NULL);
        printf("Error: Failed to execute wget. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }
}

void extract() {
    pid_t pid;
    int status;
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", ZIP_FILENAME, NULL);
        printf("Error: Failed to execute unzip. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }

}

void remove_zip() {
    pid_t pid;
    int status;
    pid = fork();
    if (pid == 0) {
        execl("/bin/rm", "rm", ZIP_FILENAME, NULL);
        printf("Error: Failed to execute rm. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else {
        printf("Error: Failed to fork.\n");
        exit(EXIT_FAILURE);
    }
}

void delete_non_manutd(){
      DIR *dir;
    struct dirent *ent;
    char path[100] = "players/";
    char buffer[256];
    char *token;
    const char *delimiter = "_";

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Failed to open directory.\n");
        exit(EXIT_FAILURE);
    }

    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
            continue;
        }

        strcpy(buffer, ent->d_name);
        token = strtok(buffer, delimiter);
        while (token != NULL) {
            if (strcmp(token, "ManUtd") == 0) {
                break;
            }
            token = strtok(NULL, delimiter);
        }

        if (token == NULL) {
            char file_path[100];
            strcpy(file_path, path);
            strcat(file_path, ent->d_name);
            remove(file_path);
        }
    }

    closedir(dir);
}

void arrange_player(int i) {
    DIR *dir;
    struct dirent *ent;
    char path[100] = "players/";
    char dir_path[100] = "/home/riskiilyas/Desktop/SISOP/players/";
    strcat(dir_path, POSISI[i]);
    char buffer[256];
    char *token;
    const char *delimiter = "_";

    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"mkdir", "-p", dir_path, NULL};
        execv("/bin/mkdir", argv);
    } else {
        wait(NULL);
    }

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error: Failed to open directory.\n");
        exit(EXIT_FAILURE);
    }

    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
            continue;
        }

        strcpy(buffer, ent->d_name);
        token = strtok(buffer, delimiter);
        while (token != NULL) {
            if (strcmp(token, POSISI[i]) == 0) {
                char file_path[100];
                strcpy(file_path, path);
                strcat(file_path, ent->d_name);
                pid = fork();
                if(pid == 0) {
                    char *argv2[] = {"cp", file_path, dir_path, NULL};
                    execv("/bin/cp", argv2);
                } else {
                    wait(NULL);
                }
                remove(file_path);
                break;
            }
            token = strtok(NULL, delimiter);
        }
    }

    closedir(dir);
}

void classify(){
    pid_t pid[4];
    for(int i=0 ;i<4; i++) {
        pid[i] = fork();
        if(pid[i]==-1) {
            perror("Failed to Create Proccess!!");
            exit(EXIT_FAILURE);
        } else if(pid[i]==0) {
            arrange_player(i);
            exit(EXIT_SUCCESS);
        }
    }

    for(int i=0;i<4;i++) {
        waitpid(pid[i], NULL, 0);
    }
}

void count_files_number(int i){
    DIR *dir;
    struct dirent *entry;
    char path[50]= "players/";
    strcat(path, POSISI[i]);

    dir = opendir(path);
    if (dir == NULL) {
        printf("Error opening directory\n");
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            PEMAIN_COUNT[i]++;
        }
    }
    closedir(dir);
}

int compare(const void *a, const void *b) {
    Player *playerA = (Player*)a;
    Player *playerB = (Player*)b;
    return playerB->score - playerA->score;
}

void swap(Player *xp, Player *yp) {
    Player temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(Player arr[], int n) {
    int i, j;
    for (i = 0; i < n-1; i++) {     
        for (j = 0; j < n-i-1; j++) {
            if (arr[j].score < arr[j+1].score) {
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

void sort_top_player(int i) {
    DIR *dir;
    struct dirent *ent;
    int fileCount = 0;
    char path[50]= "players/";
    strcat(path, POSISI[i]);
    char buffer[256];
    char *token;
    const char *delimiter = "_";

    if ((dir = opendir(path)) != NULL) {
        int ctr = 0;
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            strcpy(buffer, ent->d_name);
            token = strtok(buffer, delimiter);
            strcpy(PEMAIN[i][ctr].name, token);
            int token_ctr=0;
            while (token != NULL) {
                if(token_ctr==3) {
                    char skor[3];                    
                    strncpy(skor, token, 2);
                    skor[2] = '\0';
                    int score = atoi(skor);
                    PEMAIN[i][ctr].score = score;
                }
                token_ctr++;
                token = strtok(NULL, delimiter);
            }
            ctr++;
        }

        bubbleSort(PEMAIN[i], PEMAIN_COUNT[i]);
        closedir(dir);
    } else {
        printf("Unable to open directory\n");
    }
}

void buatTim(int bek, int gelandang, int striker) {
    char filename[256];
    sprintf(filename, "/home/riskiilyas/Formasi_%d_%d_%d.txt", bek, gelandang, striker);

    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("File tidak dapat dibuat\n");
        exit(1);
    }

    fprintf(fp, "Kiper:\n%s %d\n", PEMAIN[0][0].name, PEMAIN[0][0].score);
    
    fprintf(fp, "\nBek:\n");
    for(int i=0; i<bek; i++) {
        fprintf(fp, "%s %d\n", PEMAIN[1][i].name, PEMAIN[1][i].score);
    }

    fprintf(fp, "\nGelandang:\n");
    for(int i=0; i<gelandang; i++) {
        fprintf(fp, "%s %d\n", PEMAIN[2][i].name, PEMAIN[1][i].score);
    }

    fprintf(fp, "\nPenyerang:\n");
    for(int i=0; i<striker; i++) {
        fprintf(fp, "%s %d\n", PEMAIN[3][i].name, PEMAIN[1][i].score);
    }
 
    fclose(fp);
}

void kesebelasan(){
   for(int i=0; i<4; i++) {
    count_files_number(i);
    sort_top_player(i);
    printf("\n");
   }


   int team[4] = {0};
   team[0]++;

    int ctr=1;

    while(ctr<11) {
        int max=0, id=-1;
        for(int j=1; j<4; j++) {
            if(PEMAIN[j][team[j]].score > max) {
                max=PEMAIN[j][team[j]].score;
                id=j;
            }
        }
        if(id!=-1)team[id]++;
        ctr++;
    }

    buatTim(team[1], team[2], team[3]);
}

int main() {
    download();
    extract();
    remove_zip();
    delete_non_manutd();
    classify();
    kesebelasan();

    return 0;
}
