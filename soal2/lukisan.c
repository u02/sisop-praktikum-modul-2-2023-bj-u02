#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <zlib.h>		
#include <dirent.h>	

#define FOLDER_NAME_FORMAT "%Y-%m-%d_%H:%M:%S"
#define FILE_NAME_FORMAT   "%Y-%m-%d_%H:%M:%S.jpg"
#define FOLDER_CREATE_INTERVAL 30
#define DOWNLOAD_INTERVAL 5
#define MAX_PHOTOS 15
#define BUFFER_SIZE 1024

void create_folder() {
    time_t current_time;
    struct tm* time_info;
    char folder_name[80];

    time(&current_time);
    time_info = localtime(&current_time);
    strftime(folder_name, sizeof(folder_name), FOLDER_NAME_FORMAT, time_info);
    // created the folder
    mkdir(folder_name, 0700);
    // Change current folder_name to the newly created folder
    chdir(folder_name);
}



void generate_stop_script() {
    char kill_prog[] = "lukisan";
    FILE* script_file = fopen("stop_daemon.sh", "w");
    if (script_file == NULL) {
        perror("Failed to create stop script file");
        exit(EXIT_FAILURE);
    }

    fprintf(script_file, "#!/bin/sh\n");
    fprintf(script_file, "kill $(pgrep -f %s)\n", kill_prog);
    fprintf(script_file, "rm -f stop_daemon.sh\n");

    if (fclose(script_file) != 0) {
        perror("Failed to close stop script file");
        exit(EXIT_FAILURE);
    }

    if (chmod("stop_daemon.sh", 0700) != 0) {
        perror("Failed to set stop script file permissions");
        exit(EXIT_FAILURE);
    }

    printf("Kill script generated... \n");
}

int zip_folder(const char* folder_path, const char* zip_path) {
    DIR* dir;
    struct dirent* entry;
    char buffer[BUFFER_SIZE];
    int len, err;
    gzFile zip_file = gzopen(zip_path, "wb");

    if (!zip_file) {
        perror("Failed to create zip file");
        return -1;
    }

    dir = opendir(folder_path);
    if (!dir) {
        perror("Failed to open folder");
        gzclose(zip_file);
        return -1;
    }

    // Add each file in the folder to the zip archive
    while ((entry = readdir(dir))) {
        if (entry->d_type == DT_REG) { // Regular file
            char file_path[PATH_MAX];
            struct stat file_stat;

            // Get the file path and stat info
            snprintf(file_path, sizeof(file_path), "%s/%s", folder_path, entry->d_name);
            if (stat(file_path, &file_stat) != 0) {
                perror("Failed to get file info");
                gzclose(zip_file);
                closedir(dir);
                return -1;
            }

            // Write the file header to the zip archive
            gzprintf(zip_file, "filename: %s\n", entry->d_name);
            gzprintf(zip_file, "filesize: %ld\n", file_stat.st_size);
            gzprintf(zip_file, "filemode: %o\n", file_stat.st_mode);
            gzprintf(zip_file, "filetime: %ld\n", file_stat.st_mtime);
            gzprintf(zip_file, "filedata: \n");

            // Write the file data to the zip archive
            FILE* file = fopen(file_path, "rb");
            if (!file) {
                perror("Failed to open file for reading");
                gzclose(zip_file);
                closedir(dir);
                return -1;
            }

            while ((len = fread(buffer, 1, BUFFER_SIZE, file)) > 0) {
                if (gzwrite(zip_file, buffer, len) != len) {
                    perror("Failed to write file data to zip archive");
                    gzclose(zip_file);
                    fclose(file);
                    closedir(dir);
                    return -1;
                }
            }

            fclose(file);
            gzprintf(zip_file, "\n");
        }
    }

    closedir(dir);
    gzclose(zip_file);

    return 0;
}

void zip(char *folder_name) {
    pid_t pid;
    int status;

    // Construct the command to zip the folder_name
    char *zipArgs[] = {"zip", "-r", "-q", "-9", folder_name, folder_name, NULL};

    // Fork a child process to run the zip command
    pid = fork();
    if (pid == 0) {
        execvp(zipArgs[0], zipArgs);
        perror("zip error");
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        perror("fork error");
        exit(EXIT_FAILURE);
    }

    // Wait for the zip process to finish
    waitpid(pid, &status, 0);
}

void download_photos() {
    int photo_count = 0;
    while (photo_count < MAX_PHOTOS) {
        time_t current_time;
        struct tm* time_info;
        char file_name[80];
        char url[50];

        time(&current_time);
        time_info = localtime(&current_time);
        strftime(file_name, sizeof(file_name), FILE_NAME_FORMAT, time_info);
        sprintf(url, "https://picsum.photos/%d", (photo_count % 10) + 400);

        FILE* file = fopen(file_name, "wb");
        if (file == NULL) {
            perror("Failed to create file");
            exit(EXIT_FAILURE);
        }

        printf("Downloading %s...\n", file_name);
        fflush(stdout);

        pid_t pid = fork();
        if (pid < 0) {
            perror("Failed to fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            // Child process
			//execl("/usr/bin/wget", "wget", "-q", "-O", file_name, url, NULL);
            char size_option[20];
            sprintf(size_option, "%dx%d", (int)(current_time % 1000) + 50, (int)(current_time % 1000) + 50);
            execl("/usr/bin/wget", "wget", "-q", "-O", file_name, url, NULL);
            execl("/usr/bin/convert", "convert", "-resize", size_option, file_name, file_name, NULL);
        } else {
            // Parent process
            fclose(file);
            photo_count++;
            sleep(DOWNLOAD_INTERVAL);
        }
    }
    // Wait for child processes to complete
    while (wait(NULL) > 0) {}
}



int main() {
    char folder_name[80];
    const char* zip_path = strcat(folder_name, ".zip");    

    generate_stop_script();

    while (1) {
        
        pid_t pid = fork();
        if (pid < 0) {
            perror("Failed to fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            // Child process
            create_folder();
            download_photos();
            exit(EXIT_SUCCESS);
            chdir("..");
            zip(folder_name);
            //const char* folder_path = "/path/to/folder";
            //const char* zip_path = strcat(folder_name, ".zip");
            //const char* zip_path = "/home/borgir/praktikum/modul2/soal22"); 
            //zip_folder(folder_name, zip_path);
        } else {
            // Parent process
            // Wait for next folder create interval
            sleep(FOLDER_CREATE_INTERVAL);
        }
        
        //zip_folder(folder_name, zip_path);
        //zip(folder_name);
    }

    return 0;
}
